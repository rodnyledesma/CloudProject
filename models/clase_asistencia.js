'use strict';
module.exports = (sequelize, DataTypes) => {
  const clase_asistencia = sequelize.define('clase_asistencia', {
        external: DataTypes.UUID
  }, {freezeTableName: true});
  clase_asistencia.associate = function(models) {
    // associations can be defined here
    clase_asistencia.belongsTo(models.persona, {foreignKey: 'id'});
    clase_asistencia.belongsTo(models.clase, {foreignKey: 'id'});
  };
  return clase_asistencia;
};