'use strict';
module.exports = (sequelize, DataTypes) => {
  const estacionesTrabajo = sequelize.define('estacionesTrabajo', {
    ubicacion: DataTypes.STRING,
    disponibilidad: DataTypes.BOOLEAN,
    external: DataTypes.UUID
  }, {freezeTableName: true});
  estacionesTrabajo.associate = function(models) {
    // associations can be defined here
    estacionesTrabajo.belongsTo(models.laboratorio, {foreignKey: 'id'});
    estacionesTrabajo.hasMany(models.componente,{foreignKey:'id', as: 'componente'});
  };
  return estacionesTrabajo;
};