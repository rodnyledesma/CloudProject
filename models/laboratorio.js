'use strict';
module.exports = (sequelize, DataTypes) => {
  const laboratorio = sequelize.define('laboratorio', {
    ubicacion: DataTypes.STRING,
    numero: DataTypes.INTEGER,
    disponibilidad: DataTypes.BOOLEAN,
    external: DataTypes.UUID
  }, {freezeTableName: true});
  laboratorio.associate = function(models) {
    // associations can be defined here
    laboratorio.hasMany(models.clase,{foreignKey:'id', as: 'clase'});
    laboratorio.hasMany(models.estacionesTrabajo,{foreignKey:'id', as: 'estacionesTrabajo'});
  };
  return laboratorio;
};