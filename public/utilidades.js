function validarCedula(cedula) {
    var cad = cedula.trim();
    var total = 0;
    var longitud = cad.length;
    var longcheck = longitud - 1;

    if (cad !== "" && longitud === 10) {
        for (i = 0; i < longcheck; i++) {
            if (i % 2 === 0) {
                var aux = cad.charAt(i) * 2;
                if (aux > 9)
                    aux -= 9;
                total += aux;
            } else {
                total += parseInt(cad.charAt(i)); // parseInt o concatenará en lugar de sumar
            }
        }

        total = total % 10 ? 10 - total % 10 : 0;

        if (cad.charAt(longitud - 1) == total) {
            return true;
        } else {
            return false;
        }
    }
}

function manejoMensajes(msg) {    
    //console.log(error);
    var mensaje = '<div class="alert alert-danger">';
    mensaje += msg;
    mensaje += '</div>';
    $("#error").html(mensaje);
}

function validarc() {
    //metodos añadidos ----------------------------------------
    $.validator.addMethod("soloLetras", function (value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Solo letras");

    $.validator.addMethod("registro", function (value, element) {
        return this.optional(element) || /^[N]-[0-9]{4}-[R]-[0-9]{3}$/.test(value);
    }, "Ingrese un registro valido");

    $.validator.addMethod("cedula", function (value, element) {
        return this.optional(element) || validarCedula(value);
    }, "Cedula no valida");

    $("#formulario").validate({
        rules: {
            cedula: {
                required: true,
                minlength: 10,
                maxlength: 10,
                cedula: true,
                number: true
            },
            cargo:{
                required: true
            },
            nombres: {
                required: true,
                minlength: 3,
                maxlength: 20,
                soloLetras: true
            },
            apellidos: {
                required: true,
                minlength: 3,
                maxlength: 20,
                soloLetras: true
            },
            edad: {
                required: true,
                number: true
            },
            correo: {
                required: true,
                email: true
            },
            pword: {
                required: true,
                minlength: 2
            }
        }
    });
}


