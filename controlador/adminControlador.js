'use strict';
var models = require('./../models/');
var uuid = require('uuid');
var persona = models.persona;
class adminControlador {

   
    guardar_admin(req, res) {
        var datos = {
            cedula: req.body.ceduladm,
            nombre: req.body.nomrbedm,
            apellido: req.body.apellidodm,
            cargo: 3,
            external: uuid.v4(),
            cuenta: {
                correo: req.body.correodm,
                clave: req.body.clavedm,
                estado: 1,
                external: uuid.v4()
            }
        };
        persona.create(datos, {include: [{model: models.cuenta, as: 'cuenta'}]}).then(function (dat) {
            //Sreq.flash('ok', "Se ha registrado Persona correctamente");
            res.redirect("/listadmin");
        });
    }
/* Metodods sin ventana ******************************************************************************* 
    enlistar_admins(req, res) {
        persona.findAll({where: {cargo: 3}}).then(function (listado) {
            if (listado.length > 0) {
                var lista = listado;
                res.render('fragmentos/administrador/listadmin', {title: 'Listado de administradores', admins: lista});
            } else {
                res.redirect('/student');
            }
        }).error(function (error) {
            console.log('Problema al cargar estudiantes ');
        });
    }
    registrar_estudiante(req, res) {
        var datos = {
            cedula: req.body.cedula_est,
            nombre: req.body.nombre_est,
            apellido: req.body.apellido_est,
            fecha_nac: req.body.fecha_est,
            cargo: 1,
            external: uuid.v4(),
            cuenta: {
                correo: req.body.correo_est,
                clave: req.body.clave_est,
                estado: 1,
                external: uuid.v4()
            }
        };
        persona.create(datos, {include: [{model: models.cuenta, as: 'cuenta'}]}).then(function (dat) {
            //Sreq.flash('ok', "Se ha registrado estudiante correctamente");
        });
    }

    enlistar_estudiantes(req, res) {
        persona.findAll({where: {cargo: 1}}).then(function (listado) {
            if (listado.length > 0) {
                var lista = listado;
                res.render('fragmentos/administrador/liststudent', {title: 'Listado de estudiantes', estudiantes: lista});
            } else {
                console.log('aun no hay estudiantes');
                res.redirect('/student');
            }
        }).error(function (error) {
            console.log('Problema al cargar estudiantes ');
        });
    }

    nuevo_modulo(req, res) {
        
    }

    nueva_estacioTrabajo(req, res) {
        
    }
    
    guardar_docente(req, res) {
        var datos = {
            cedula: req.body.ceduladm,
            nombre: req.body.nomrbedm,
            apellido: req.body.apellidodm,
            fecha_nac: req.body.fechadm,
            cargo: 2,
            external: uuid.v4(),
            cuenta: {
                correo: req.body.correodm,
                clave: req.body.clavedm,
                estado: 1,
                external: uuid.v4()
            }
        };
        persona.create(datos, { include: [{ model: models.cuenta, as: 'cuenta' }] }).then(function (dat) {
            //Sreq.flash('ok', "Se ha registrado Persona correctamente");
            res.redirect("/listadmin");
        });
    }
*********************************************************************************************/
}
module.exports = adminControlador;

